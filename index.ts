const got = require('got')
const auth = require('./auth')

const yargs = require('yargs/yargs')
const { hideBin } = require('yargs/helpers')

yargs(hideBin(process.argv))
.commandDir('cmds', { extentions: ['js'] })
.demandCommand()
.argv
// yargs(hideBin(process.argv))
//   .command('serve [port]', 'start the server', (yargs) => {
//     return yargs
//       .positional('port', {
//         describe: 'port to bind on',
//         default: 5000
//       })
//   }, (argv) => {
//     if (argv.verbose) console.info(`start server on :${argv.port}`)
//     serve(argv.port)
//   })
//   .option('verbose', {
//     alias: 'v',
//     type: 'boolean',
//     description: 'Run with verbose logging'
//   })
//   .parse()



type problem = {
    contestId: number,
    index: string,
    name: string,
    type: string,
    tags: any
}

type problemStat = {
    contestId: number,
    index: string,
    solvedCount: number
}

type problemsRensponse = {
    problems: problem[],
    problemStatistics: problemStat[]
}

type response = {
    status: any,
    result: problemsRensponse
}

;(async () => {
const res = await got('https://codeforces.com/api/problemset.problems', { responseType: 'json' })
const obj: problemsRensponse = res.body.result
console.log(obj.problems.slice(undefined, 5).map((problem) => problem.name).join('\n'))
})
