// apiKey = key
// time = unix time
// apiSig = ??????
// https://codeforces.com/api/contest.hacks?contestId=566&apiKey=xxx&time=1638813022&apiSig=123456<hash>
// , где <hash> равен sha512Hex(123456/contest.hacks?apiKey=xxx&contestId=566&time=1638813022#yyy)

const sha512 = require('sha512')

type AuthOpts = {
  method: string,
  key: string,
  secret: string,
  params: any[]
}

function getSignedParams ({ method, key, secret, params }: AuthOpts) {
  params.push({ apiKey: key }, { time: (Math.floor(Date.now() / 1000)) })
  params.sort((a, b) => Object.keys(a)[0].localeCompare(Object.keys(b)[0]))
  const apiSig = '222222' + sha512('222222/' + method + '?' + params.map((param) => Object.keys(param)[0] + '=' + param[Object.keys(param)[0]]).join('&') + '#' + secret).toString('hex')
  params.push({ apiSig })
  return params
}

module.exports = {
  getSignedParams
}